<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Planeta de los Simios</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
	<div class="container" >
		<div class="row text-center justify-content-center align-items-center " style="  height: 100vh;">
			<div class="col-12">
				<form action="" method="POST">
					<?php
						if(isset($errorLogin)){
							echo $errorLogin;
						}
					?>
					<h2 class="text-primary">Planeta de los Simios</h2>
					<p>Nombre de usuario: <br>
					<input type="text" name="username"></p>
					<p>Password: <br>
					<input type="password" name="password"></p>
					
					<button type="submit" class="btn btn-primary" value="Iniciar Sesión">Iniciar Sesion</button>
				</form>
			</div>
		</div>
	</div>
</body>	